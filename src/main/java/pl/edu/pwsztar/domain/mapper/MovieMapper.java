package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper {
    public Movie mapToMovie(CreateMovieDto movieDTO) {

        Movie movie = new Movie();

        movie.setTitle(movieDTO.getTitle());
        movie.setImage(movieDTO.getImage());
        movie.setYear(movieDTO.getYear());

        return movie;
    }
}
